This is learning app.
I want to learn to use Hibernate.

I followed this tutorial.
https://www.journaldev.com/2924/hibernate-one-to-many-mapping-annotation

Database MySQL
DB tool: MySQL Workbench

CREATE SCHEMA `car_hire_schema` ;

=====================================================

CREATE TABLE `car_hire_schema`.`customer` (
  `customer_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_name` VARCHAR(45) NOT NULL,
  `customer_details` VARCHAR(45) NULL,
  `gender` INT NOT NULL,
  `email_address` VARCHAR(45) NULL,
  `phone_number` VARCHAR(45) NULL,
  PRIMARY KEY (`customer_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

=====================================================

CREATE TABLE `car_hire_schema`.`booking` (
  `booking_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_from` DATETIME NOT NULL,
  `date_to` DATETIME NOT NULL,
  `confirmation_sent` DATETIME NULL,
  `payment_received` DATETIME NULL,
  `customer_id` INT(10) UNSIGNED NULL,
  PRIMARY KEY (`booking_id`),
  INDEX `fk_booking_customer_idx` (`customer_id` ASC),
  CONSTRAINT `fk_booking_customer`
    FOREIGN KEY (`customer_id`)
    REFERENCES `car_hire_schema`.`customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

=====================================================

TODO:
    tests, for example check of DB connection from GUI = rest
===
FOR TESTING there is SoapUi project,
you can find it here: src/main/resources/SoapUi/*.xml

*.jar file runs on WildFly 10 for example.