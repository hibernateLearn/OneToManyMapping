package cz.ill_j.hibernate.main;

import cz.ill_j.hibernate.model.Booking;
import cz.ill_j.hibernate.model.Customer;
import cz.ill_j.hibernate.model.GenderEnum;
import cz.ill_j.hibernate.util.DateUtils;
import cz.ill_j.hibernate.util.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * this class was created for devel testing only <br>
 * contains main method which inserts new record to MySql DB
 */
public class OneToMany {

    public static void main(String[] args) {

        // to prepare Date object
        DateUtils dateUtils = new DateUtils();

        // create new customer
        Customer customer = new Customer();
        customer.setName("Joe Doe");
        customer.setDetails("Joe sleeps with his secretary.");
        customer.setGender(GenderEnum.MAN.getValue());

        // booking object setting 2x
        Booking booking01 = new Booking();
        booking01.setDateFrom(dateUtils.getFormatedDate("2014-02-11"));
        booking01.setDateTo(dateUtils.getFormatedDate("2014-03-01"));
        booking01.setCustomer(customer);

        Booking booking02 = new Booking();
        booking02.setDateFrom(dateUtils.getFormatedDate("2015-04-21"));
        booking02.setDateTo(dateUtils.getFormatedDate("2015-04-28"));
        booking02.setCustomer(customer);

        // mapping
        List<Booking> setBookings = new ArrayList();
        setBookings.add(booking01);
        setBookings.add(booking02);

        customer.setBookings(setBookings);

        // save to DB
        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction transaction = null;

        try {
            // get session
            sessionFactory = HibernateUtils.getSessionFactory();
            session = sessionFactory.getCurrentSession();
            System.out.println("Session created.");
            // start transaction
            transaction = session.beginTransaction();
            // save model
            session.save(customer);
            session.save(booking01);
            session.save(booking02);
            // commit
            transaction.commit();
            // print ID values
            System.out.println("New customer ID: " + customer.getId());
            System.out.println("booking01 ID: " + booking01.getId() + ", Foreign key customer ID: " + booking01.getCustomer().getId());
            System.out.println("booking02 ID: " + booking02.getId() + ", Foreign key customer ID: " + booking02.getCustomer().getId());
        } catch (Exception e) {
            System.out.println("Exception occured. "+e.getMessage());
            e.printStackTrace();
        } finally {
            if (!sessionFactory.isClosed()) {
                System.out.println("Closing SessionFactory");
                sessionFactory.close();
            }
        }

    }

}
