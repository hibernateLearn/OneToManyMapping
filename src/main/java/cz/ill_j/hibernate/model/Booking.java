package cz.ill_j.hibernate.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="booking")
public class Booking {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="booking_id")
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_from")
    private Date dateFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_to")
    private Date dateTo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="confirmation_sent")
    private Date confirmationSentDateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="payment_received")
    private Date paymentReceivedDateTime;

    @ManyToOne
    @JoinColumn(name="customer_id", nullable = false)
    @JsonIgnore
    private Customer customer;

    // getters & setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Date getConfirmationSentDateTime() {
        return confirmationSentDateTime;
    }

    public void setConfirmationSentDateTime(Date confirmationSentDateTime) {
        this.confirmationSentDateTime = confirmationSentDateTime;
    }

    public Date getPaymentReceivedDateTime() {
        return paymentReceivedDateTime;
    }

    public void setPaymentReceivedDateTime(Date paymentReceivedDateTime) {
        this.paymentReceivedDateTime = paymentReceivedDateTime;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
