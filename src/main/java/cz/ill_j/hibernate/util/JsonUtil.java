package cz.ill_j.hibernate.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import cz.ill_j.hibernate.model.Customer;
import cz.ill_j.hibernate.rest.RestService;
import org.apache.log4j.Logger;

import java.util.List;

public class JsonUtil {

    private static final Logger logger = Logger.getLogger(RestService.class);

    public String getJsonStringFromArrayList(List<Customer> list) {
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = null;
        try {
            jsonString = objectMapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            logger.error("Write value as String failed.", e);
        }
        return jsonString;
    }
}
