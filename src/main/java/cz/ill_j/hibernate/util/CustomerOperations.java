package cz.ill_j.hibernate.util;

import cz.ill_j.hibernate.model.Customer;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * CRUD for Customer object
 */
public class CustomerOperations {

    private static final Logger logger = Logger.getLogger(CustomerOperations.class);

    /**
     *
     * @param customer
     * @return new customer ID
     */
    public long createCustomer(Customer customer) {

        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction transaction = null;

        try {
            // get session
            sessionFactory = HibernateUtils.getSessionFactory();
            session = sessionFactory.getCurrentSession();
            System.out.println("Session created.");
            // start transaction
            transaction = session.beginTransaction();
            // save model
            session.save(customer);
            // commit
            transaction.commit();
            // print ID values
            logger.info("New customer ID: " + customer.getId());
        } catch (Exception e) {
            logger.info("\n.......Transaction Is Being Rolled Back.......\n");
            session.getTransaction().rollback();
            logger.error("Exception occured. ", e);
        } finally {
            if (sessionFactory != null) {
                logger.info("Closing SessionFactory");
                sessionFactory.close();
            }
        }

        return customer.getId();
    }

    public List<Customer> readAllCustomers() {
        List<Customer> customerList = new ArrayList();

        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction transaction = null;

        try {
            // get session
            sessionFactory = HibernateUtils.getSessionFactory();
            session = sessionFactory.getCurrentSession();
            System.out.println("Session created.");
            // start transaction
            transaction = session.beginTransaction();
            customerList = session.createQuery("FROM Customer").list();
            logger.info("Customer list has size " + customerList.size());
        } catch (Exception e) {
            logger.error("Exception occured. ", e);
        } finally {
            if (session != null) {
                logger.info("Closing Session.");
                session.close();
            }
        }
        return customerList;
    }

    public void updateCustomer(long id) {

    }

    public void deleteCustomer(long id) {

    }
}
