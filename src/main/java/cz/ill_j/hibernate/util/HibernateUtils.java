package cz.ill_j.hibernate.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import org.jboss.logging.Logger;


public class HibernateUtils {

    private static final Logger logger = Logger.getLogger(HibernateUtils.class);
    // configuration for XML way
    private static SessionFactory sessionFactory;

    // configuration for JPA way
    private static SessionFactory sessionFactoryAnnotation;

    // Property based config
    private static  SessionFactory sessionJavaConfigFactory;

    private static SessionFactory buildSessionFactory() {

        String resource = "hibernate-annotation.cfg.xml";
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            Configuration configuration = new Configuration();
            configuration.configure(resource);
            String msg = "Hibernate Annotation Configuration loaded.";
            System.out.println(msg);
            logger.info(msg);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            System.out.println("Hibernate serviceRegistry created");

            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            return sessionFactory;
        } catch (Throwable e) {
            logger.error("Hibernate configuration error. Loading: " + resource, e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory() {
        if(sessionFactory == null) {
            sessionFactory = buildSessionFactory();
        }
        return sessionFactory;
    }
}
