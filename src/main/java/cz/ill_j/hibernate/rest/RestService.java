package cz.ill_j.hibernate.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.ill_j.hibernate.model.Customer;
import cz.ill_j.hibernate.util.CustomerOperations;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/RestService")
@ApplicationPath("/onetomany")
public class RestService extends Application {

    private static final Logger logger = Logger.getLogger(RestService.class);

    // http://localhost:8080/onetomanymapping/onetomany/RestService/sayHello
    @GET
    @Path("/sayHello")
    public String getTestMessage() {
        return "Hello World! ";
    }

    @POST
    @Path("/sayParam")
    public String getTestMessageWithParam(@QueryParam("name") String name) {
        return getTestMessage() + name;
    }

    /**
     *
     * @param jsonData (has to contain parameter "name" at least)
     * @return Response with new customer ID
     */
    @POST
    @Path("/addCustomer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCustomer(String jsonData) {
        String result = "Data: " + jsonData;
        logger.info("String result = \"Data: \"" + jsonData);

        // parse String to byte array
        byte[] jsonBytes = jsonData.getBytes();
        // create ObjectMapper instance
        ObjectMapper mapper = new ObjectMapper();
        Customer customer = null;
        try {
            logger.info("Try to parse Json to Object.");
            customer = mapper.readValue(jsonBytes, Customer.class);
        } catch (Exception e) {
            logger.error("Can't create Customer object from JSON.", e);
        }
        CustomerOperations customerOperations = new CustomerOperations();
        logger.info(customer.toString());

        long id = customerOperations.createCustomer(customer);

        // TODO: add condition to return fault code
        return Response.status(200).entity(id).build();
    }

    @GET
    @Path("/getAllCustomers")
    @Produces("application/json")
    public Response getAllCustomers() {
        CustomerOperations customerOperations = new CustomerOperations();
        List<Customer> list = customerOperations.readAllCustomers();

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = null;
        try {
            jsonString = objectMapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            logger.error("Write value as String failed.", e);
        }
        return Response.ok(jsonString).build();
    }

    @GET
    @Path("/getCustomers")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getCustomers() {
        final List<Customer> customerList = new CustomerOperations().readAllCustomers();

        final GenericEntity<List<Customer>> entity
                = new GenericEntity<List<Customer>>(customerList) {};
        return Response.ok().entity(customerList).build();
    }
}
