package testmain;

import cz.ill_j.hibernate.model.Booking;
import cz.ill_j.hibernate.model.Customer;
import cz.ill_j.hibernate.util.CustomerOperations;
import cz.ill_j.hibernate.util.JsonUtil;

import java.util.List;

public class LoadRecords {

    public static void main(String[] args) {
        List<Customer> list = new CustomerOperations().readAllCustomers();
        JsonUtil util = new JsonUtil();

        System.out.println("Testing, load all Customers.");
        for (Customer customer : list) {
            System.out.println("Customer name: " + customer.getName());
            System.out.println("... load Booking object.");

            for (Booking booking : customer.getBookings()) {
                System.out.println("Booking ID: " + booking.getId());
            }
        }

        System.out.println("Try to parse List<Customer> to JSON.");
        System.out.println("...");
        System.out.println(util.getJsonStringFromArrayList(list));
    }
}
